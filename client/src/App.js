import React, { useEffect } from 'react';
import './App.css';
import Header from './features/mainPage/header/header';
import { Routes,Route, useLocation} from "react-router-dom";
import MainPage from './features/mainPage/mainPage';
import Register from './features/LoginRegister/register/register';
import Login from './features/LoginRegister/login/login';
import ForgetPassword from './features/LoginRegister/forgetPassword/forget';
import NewPassword from './features/LoginRegister/newPassword/newPassword';
import Fortuna from './features/mainPage/fortuna/fortuna';
import "./features/mainPage/fortuna/fortuna.css"
import Myvidjet from './features/myvidjet/myvidjet';
import Aplication from './features/aplication/aplication';
import Payment from './features/payment/payment';
import Cooperation from './features/cooperation/cooperation';
// import SettingWidjet from './features/myvidjet/settings/settingsWidjet';
import Privat from './features/Privat/Privat';
import { useDispatch, useSelector } from 'react-redux';
import { authenticationToken } from './features/LoginRegister/asyncLogin';



function App() {
  const loginIsGoodAuth = useSelector((state) => state.fortuna.loginIsGoodAuth)
  const dispatch = useDispatch()
  const history = useLocation()
  const pathName = history.pathname === "/login" || history.pathname === "/register" 
  || history.pathname === "/forgetPassword" || history.pathname === "/newPassword" || history.pathname === "/payment" || history.pathname === "/PrivatiPolic"
  || history.pathname === "/cooperation"  || history.pathname === "/myVidjet" || history.pathname === "/aplication";
  const pathNameAnother = history.pathname === "/myVidjet" || history.pathname === "/payment" 
  || history.pathname === "/aplication" || history.pathname === "/cooperation" || history.pathname === "/PrivatiPolic";
  const pathForMenyu = history.pathname === "/register" || history.pathname === "/login" || history.pathname === "/forgetPassword" || history.pathname === "/newPassword"

  // useEffect(() => {
  //   dispatch(authenticationToken())
  // })
  useEffect(() =>{ dispatch(authenticationToken()) },[])

  
  return (
      <section className='App'>
        <Header path={pathName} pathNameAnother={pathNameAnother} pathforMenyu={pathForMenyu}/>
        <Routes>
          <Route index element={<MainPage/>}/>
          <Route path='/register' element={<Register/>}/>
          <Route path='/login' element={<Login/>}/>
          <Route path='/forgetPassword' element={<ForgetPassword/>}/>
          {/* <Route path='/newPassword' element={<NewPassword/>}/> */}
          <Route path='/newPassword/:token' element={<NewPassword/>}/>
          <Route path='/onlyFortuna' element={<Fortuna/>}/>
          <Route path='/myVidjet' element={ <Myvidjet/> }/>
          <Route path='/aplication' element={ <Aplication/> }/>
          <Route path='/payment' element={ <Payment/> }/>
          <Route path='/cooperation' element={ <Cooperation/> }/>
          <Route path='/PrivatiPolic' element={ <Privat/> }/>
        </Routes>
      </section>
    );
  }

  

export default App;
