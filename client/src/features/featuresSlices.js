import { createSlice } from "@reduxjs/toolkit";

import oneIcon from "../srcImages/group.png";
import twoIcon from "../srcImages/group2.png";
import threeIcon from "../srcImages/group3.png";

import paypal from "../srcImages/paypal.svg";
import stripe from "../srcImages/stripe.svg";
import visa from "../srcImages/visa.svg";
import master from "../srcImages/master.svg";
import arca from "../srcImages/arca.svg";
import vidjetEye from "../srcImages/vidjetEye.svg";
import filter from "../srcImages/filter.svg";
import tools from "../srcImages/tools.svg";
import copy from "../srcImages/copy.svg";
import deleted from "../srcImages/delete.svg";
import { checkExpiration, createNewPassword, faceBookLogin, forgetPassword, forgetPasswordfetch, authenticationToken, loginFetch, logOuth, registerFetch, sendEmail } from "./LoginRegister/asyncLogin";



let initialState = {
   fortunaCircleSlice:[
       {id:1,positionTop:"-47px",positionRight:"263px"},
       {id:2,positionTop:"42px",positionRight:"42px"},
       {id:3,positionTop:"260px",positionRight:"-47px"},
       {id:4,positionTop:"477px",positionRight:"42px"},
       {id:5,positionTop:"568px",positionRight:"259px"},
       {id:6,positionTop:"477px",positionRight:"480px"},
       {id:7,positionTop:"260px",positionRight:"568px"},
       {id:8,positionTop:"42px",positionRight:"480px"},
   ],
   fortunaPriceSlices:[
        {id:7,color:"#DD3500", label:"Չի մասնակցում7"},
        {id:6,color:"#46B900", label:"Չի մասնակցում6"},
        {id:5,color:"#DD3500", label:"Չի մասնակցում5"},
        {id:4,color:"#46B900", label:"Չի մասնակցում4"},
        {id:3,color:"#6100FF", label:"Չի մասնակցում3"},
        {id:2,color:"#DD3500", label:"Չի մասնակցում2"},
        {id:1,color:"#46B900", label:"Չի մասնակցում1"},
        {id:0,color:"#6100FF", label:"Չի մասնակցում0"}
    ],
    genAplication:[
        {id:1,title:"Հայտերի սերունդ",text:"Ֆորտունայի անիվ մեծացնում է դարձի է հայտի եւ պահպանում է հեռացող այցելուներին. Փորձեք այն անվճար!",color:"#FFB800",src:oneIcon},
        {id:2,title:"Հայտերի սերունդ",text:"Ֆորտունայի անիվ մեծացնում է դարձի է հայտի եւ պահպանում է հեռացող այցելուներին. Փորձեք այն անվճար!",color:"#EA3152",src:twoIcon },
        {id:3,title:"Հայտերի սերունդ",text:"Ֆորտունայի անիվ մեծացնում է դարձի է հայտի եւ պահպանում է հեռացող այցելուներին. Փորձեք այն անվճար!",color:"#46B900",src:threeIcon}
    ],
    GuidforPopUp:[
        {id:1,title:"Ինչ է Test Drive.",description:"Ֆորտունայի անիվ մեծացնում է դարձի է հայտի եւ պահպանում է հեռացող այցելուներին. Փորձեք այն անվճար!",open:true},
        {id:2,title:"Ինչ է Test Drive.",description:"Ֆորտունայի անիվ մեծացնում է դարձի է հայտի եւ պահպանում է հեռացող այցելուներին. Փորձեք այն անվճար!",open:false},
        {id:3,title:"Ինչ է Test Drive.",description:"Ֆորտունայի անիվ մեծացնում է դարձի է հայտի եւ պահպանում է հեռացող այցելուներին. Փորձեք այն անվճար!",open:false},
        {id:4,title:"Ինչ է Test Drive.",description:"Ֆորտունայի անիվ մեծացնում է դարձի է հայտի եւ պահպանում է հեռացող այցելուներին. Փորձեք այն անվճար!",open:false}
    ],
    TarifPrice:[
        {id:1,price:0,yearPrice:50,title:"Test Drive",borderColor:"#46B900",form:10,vidjet:1},
        {id:2,price:10,yearPrice:100,title:"Test Drive",borderColor:"#FFC700",form:10,vidjet:1},
        {id:3,price:100,yearPrice:500,title:"Test Drive",borderColor:"#EA3152",form:10,vidjet:1},
    ],
    footerImg:[paypal,stripe,visa,master,arca],
    vidjetProperty:[
        {id:1,button:true,text:"Նոր վիդջեթ",eyeImgsvg:vidjetEye,filtersvg:filter,toolssvg:tools,repeatsvg:copy,deletsvg:deleted},
        {id:2,button:false,text:"Նոր վիդջեթ",eyeImgsvg:vidjetEye,filtersvg:filter,toolssvg:tools,repeatsvg:copy,deletsvg:deleted}

    ],
    aplication:{
        header:["#","Ամսաթիվ","Կապ","Բոնուս","URL"],
        content:[
            { id:1,count:1, date:"-", Contact:"-", Bonus:"-",Url:"-" },
        ]
    },
    partnerprogram:[
        {id:1,registerCount:0,paymentCount:0,BalanceCount:0}
    ],
    PayCount:[
        {id:1,TariffstandardMount:"0000",ExperttariffMount:"0000",TariffstandardYear:"0000",ExperttariffYear:"0000"}
    ],
    privaciPolic:[
        {
            id:1,title:"Գաղտնիության քաղաքականություն",text:[`Մենք պահպանում ենք անձնական տվյալների պաշտպանության և երրորդ անձանց 
            չարտոնված հասանելիությունից Ձեր տվյալները պաշտպանելու կանոնները (անձնական տվյալների պաշտպանություն) ։ Մեր վեբ-ծառայության հավելվածում մուտքագրումը նշանակում է
            անվերապահ համաձայնություն սույն Գաղտնիության քաղաքականության և դրանում նշված անձնական տեղեկատվության մշակման պայմանների հետ։`]
        },
        {
            id:2,title:"1. Անձնական տվյալներ. Անձնական տվյալների հավաքման եւ մշակման նպատակը:",text:[`1.1. Դուք միշտ կարող եք այցելել այս էջը, առանց բացահայտելու որեւէ անձնական տեղեկություններ.`,
            `1.2. Մեր ընկերությունը հավաքում եւ օգտագործում է անձնական տվյալներ, որոնք անհրաժեշտ են Ձեր հարցումը կատարելու համար, Դա անձնական ID Vkontakte-ն է կամ էլեկտրոնային փոստի հասցեն:`,
            `1.3. Մեր ընկերությունը չի ստուգում ֆիզիկական անձանց կողմից տրամադրվող անձնական տվյալների հավաստիությունը և չի ստուգում նրանց գործունակությունը ։`]
        },
        {
            id:3,title:"2. Օգտագործողի անձնական տեղեկատվության մշակման եւ երրորդ անձանց փոխանցման պայմանները:",text:[`2.1. Ծառայության օգտատերերի անձնական տվյալները մշակելիս Մեր ընկերությունը ղեկավարվում է "անձնական տվյալների մասին"ՌԴ Դաշնային օրենքով:`,
            `2.3. Մեր ընկերությունը չի փոխանցում անձնական տվյալները երրորդ անձանց.`,]
        },
        {
            id:4,title:"3. Օգտատերերի անձնական տեղեկատվության պաշտպանության համար կիրառվող միջոցները:",text:[`Մեր ընկերությունը ձեռնարկում է անհրաժեշտ և բավարար կազմակերպչական և տեխնիկական միջոցներ ՝ Օգտատիրոջ անձնական տեղեկատվությունը ոչ իրավաչափ կամ պատահական հասանելիությունից, ոչնչացումից, փոփոխությունից, արգելափակումից, պատճենումից, տարածումից պաշտպանելու, ինչպես նաև երրորդ անձանց հետ այլ ոչ իրավաչափ գործողություններից պաշտպանելու համար:`]
        },
    ],
    mainSettings:[
        {id:1,title:"Վիդջեթի գույնը",value:"#46B900"},
        {id:2,title:"Վայրկյան առաջ ավտո-բացման",value:45},
        {id:3,title:"Ինչ տվյալներ ենք հավաքում?",value:"Հեռախոսահամար"},
        {id:4,title:"Վերնագիր",value:"պտտեք անիվը!"},
        {id:5,title:"Ենթավերնագիր",value:"Մուտքագրեք ձեր հեռախոսահամարը/email հաղթելու մրցանակը!"},
        {id:6,title:"Հաղորդագրություն հաղթելուց հետո",value:"Շնորհավորում ենք! Մի բաց թողեք զանգը, մենք շուտով կկապվենք ձեզ հետ:"},
        {id:7,title:"Հղում դեպի գաղտնիության քաղաքականություն",value:"https://test.ru/politics"},
        {id:8,title:"Տարածաշրջանի հեռախոսահամարը լռելյայն",value:"Armenia (Հայաստան) (+374)"},
        {id:9,title:"Գործողության կոճակ",value:"Կոճակը անջատված է"},
    ],
    bonusSettings:[
        {id:7,title:"Բոնուս #1"},
        {id:6,title:"Բոնուս #2"},
        {id:5,title:"Բոնուս #3"},
        {id:4,title:"Բոնուս #4"},
        {id:3,title:"Բոնուս #5"},
        {id:2,title:"Բոնուս #6"},
        {id:1,title:"Բոնուս #7"},
        {id:0,title:"Բոնուս #8"},
    ],
    insertSettings:[
        {
            id:1,title:"Հայտերի ուղարկում VK-ին",value:"Հղում դեպի Vk անձնագիրը",description:`Նոր հայտեր կգան անձնական հաղորդագրություններ։ 
            Հաշիվը կապելու համար բացեք @lp9bot համայնքը: Սեղմեք avatar-ի տակ '"ավելին" → "թույլատրել հաղորդագրությունները" եւ նշեք ստորեւ բերված վանդակում VK պրոֆիլի հղումը:`
        },
        {
            id:2,title:"Հայտերի ուղարկում Telegram-ին",value:"Telegram ID",description:`Նոր հայտերը կգան Telegram-ում: Կապել Ձեր հաշիվը, բացել բոտը @lp9_bot. Սեղմեք "start" 
            կոճակը եւ նշեք ձեր ID-ն ստորեւ: Եթե մեր բոտը լռում է, ապա իմացեք ձեր ID-ն համակարգային բոտից @chatid_echo_bot`
        },
        {
            id:3,title:"Դիմումների ուղարկում Բիթրիքս24",value:"https://name.bitrix24.ru",value2:"https://test.bitrix24.ru/rest/1/tzarcfdsd12e4k/profile/", description:`Նշեք հղումը ձեր Beatrix24 եւ սեղմեք կոճակը կարգավորել webhack. Ի Bitrichs24 
            սեղմեք "Ավելացնել webhook" → "ներգնա webhook". Ստեղծել webhook հետ "CRM"իրավունքների`,description2:`Տեղադրեք այստեղ webhook-ի էջից "Օրինակ URL-ը REST-ի համար" տողը`
        },
        {   
            id:4,title:"Яндекс.Метрика",value:"Նշեք counter id",description:`Մենք ուղարկում ենք lp9_open-ի նպատակները, երբ բացվում է widget-ը եւ lp9_send-ը, երբ դիմումը ուղարկվում է: Ի Yandex.Մետրային 
            նավարկում դեպի "կարգավորում" (նպատակ էջանշանը) եւ սեղմեք "Ավելացնել նպատակ" կոճակը: "Վերնագիր" դաշտում նշեք ստեղծվող նպատակի անունը: Սահմանել switch է "Javascript-միջոցառումը" դիրքորոշումը. 
            Նշեք թիրախ id. Սեղմեք "Ավելացնել նպատակ" կոճակը եւ պահեք փոփոխությունները:`
        },
        {
            id:5,title:"Ռեթարգեթինգ ВКонтакт",value:"Նշեք Pixel id (VK-RTRG-12345-ABCDEF)",description:`Գովազդային կաբինետում, գնացեք "retargeting" → "pixel" էջը եւ սեղմեք "Ստեղծել Pixel": Պատճենեք ոչ թե կոդը, 
            այլ Pixel ID-ն JavaScript API-ի համար: Գնալ լսարանի էջանիշը եւ սեղմեք "Ստեղծել լսարան": "Կանոն" դաշտում ընտրեք "որոշակի էջեր այցելած օգտատերերը": Մենք ուղարկում ենք lp9_open-ի նպատակները, երբ բացվում 
            է widget-ը եւ lp9_send-ը, երբ դիմումը ուղարկվում է: Ընտրանքների դաշտում Ընտրեք միջոցառումը համընկնում է, նշեք ցանկալի նպատակը եւ սեղմեք "Ավելացնել նպատակը"կոճակը:`
        },
        {id:6,title:"Արտաքին URL",value:"Նշեք URL",description:`Նշված URL կգա POST հարցում տվյալների: name - անունը widget, lead-կոնտակտ, բոնուս - հաղթող մրցանակը, time-հաղթող ժամանակ.`},
    ],
    codeSettings:[
        {
            id:1,title:"վիդջեթի կոդը",value:`<script>(function(d, w){ w.lp9 = 'gozota02'; var s = d.createElement('script'); s.async = true; s.src = 
            'https://lp9.ru/widget/'+w.lp9+'.js?'+Date.now(); s.charset = 'UTF-8'; if (d.head) d.head.appendChild(s); })(document, window);</script>`
            ,description:"Ավելացրեք այս կոդը կայքում, նախքան < / body>"
        },
    ],
    registerIsGoodAuth:false,
    loginIsGoodAuth:false,
    forgetPasswordAuth:false,
    passwordChanged_done:{message:"",done:false,changed:false}
}


const featureSlices = createSlice({
    name:"fortuna",
    initialState,

    reducers:{
        openDescripotion:(state,action) => {
            state.GuidforPopUp = state.GuidforPopUp.map((val) => {
                if(val.id === action.payload.id){ return {...val,open:!val.open}} 
                else {return {...val,open:false}}
            })
        },
        setTarif:(state,action) => {
            state.vidjetProperty = state.vidjetProperty.map((val) => {
                if(val.id === action.payload.id){ return {...val,button:!val.button}} 
                else {return {...val,button:false}}
            })
        },
        createVidjet:(state) => {
            state.vidjetProperty.push({id:state.vidjetProperty.length + 1,button:false,text:"Նոր վիդջեթ",eyeImgsvg:vidjetEye,filtersvg:filter,toolssvg:tools,repeatsvg:copy,deletsvg:deleted})
        },
        deleteVidjet:(state,action) => {
            state.vidjetProperty = state.vidjetProperty.filter((val) => {
                if(val.id !== action.payload.id) {
                    return  val
                } 
            })
        },
        setColorRedux:(state,action) => {
            state.fortunaPriceSlices = state.fortunaPriceSlices.map((val,i) => {
                if(i % 2 === 0 ) {
                    return { ...val, color:action.payload.color.hex }
                } else {
                    return { ...val, color:action.payload.color.hex + "70" }
                }

            })
            console.log(action.payload.color)
        },
        setPriceName:(state,action) => {
            state.fortunaPriceSlices = state.fortunaPriceSlices.map((val) => {
                if(val.id === action.payload.id) {
                    return  {...val,label:action.payload.value}
                }
                return val
            })
        },
        setRegisterPath:(state,action) => {
            state.registerIsGoodAuth = false
        },
        setLoginPath:(state,action) => {
            state.loginIsGoodAuth = false
        },
        confEmail:(state,action) => {
            state.forgetPasswordAuth = false
        },
        confirePasswordChange:(state,action) => {
            state.passwordChanged_done = {message:"",done:false,changed:false}
        }
    },

    
    extraReducers:(builder) => {
        builder
        .addCase(registerFetch.pending,(state,action) => {
            console.log("pending register",action)
        })
        .addCase(registerFetch.fulfilled,(state,action) => {
            console.log("fulfiled register",action)
            state.registerIsGoodAuth = true
        })
        .addCase(registerFetch.rejected,(state,action) => {
            console.log("fulfiled rejected",action);
            state.registerIsGoodAuth = null
        })
        .addCase(loginFetch.pending,(state,action) => {
            console.log("pending login",action);
            state.loginIsGoodAuth = "loading";
        })
        .addCase(loginFetch.fulfilled,(state,action) => {
            console.log("fulfiled login",action);
            switch(action.payload.message) {
                case undefined : state.loginIsGoodAuth = "Good"; break;
                case "falseEmail" : state.loginIsGoodAuth = "falseEmail"; break;
                case "falsePassword" : state.loginIsGoodAuth = "falsePassword"; break;
            }
        })
        .addCase(sendEmail.fulfilled,(state,action) => {
            console.log("fulfiled sendEmail",action);
            switch(action.payload.message) {
                case "emailForgetTrue" : state.forgetPasswordAuth = "emailForgetTrue"; break;
                case "forgetFalse" : state.forgetPasswordAuth = "forgetFalse"; break;
            }
        })
        .addCase(logOuth.pending,(state,action) => {
            console.log("pending Logouth",action);
        })
        .addCase(logOuth.fulfilled,(state,action) => {
            console.log("fulfiled LogOuth",action);
        })
        .addCase(createNewPassword.pending,(state,action) => {
            console.log("pending createNewPassword",action);
        })        
        .addCase(createNewPassword.fulfilled,(state,action) => {
            console.log("fulfiled createNewPassword",action);
            state.passwordChanged_done = action.payload
        })
        .addCase(checkExpiration.pending,(state,action) => {
            console.log("pending checkToken",action);
        })        
        .addCase(checkExpiration.fulfilled,(state,action) => {
            console.log("fulfiled checkToken",action);
            state.passwordChanged_done = action.payload
        })
        .addCase(faceBookLogin.pending,(state,action) => {
            console.log("pending faceBookLogin",action);
        })        
        .addCase(faceBookLogin.fulfilled,(state,action) => {
            debugger
            console.log("fulfiled faceBookLogin",action);
        })
        .addCase(authenticationToken.pending,(state,action) => {
            console.log("pending authenticationToken",action);
            state.loginIsGoodAuth = "loading";

        })
        .addCase(authenticationToken.fulfilled,(state,action) => {
            console.log("fulfiled authenticationToken",action);
            switch(action.payload.success) {
                case true : state.loginIsGoodAuth = "Good"; break;
                case false : state.loginIsGoodAuth = false; break;
            }
        })
    }
})

export const {
    openDescripotion,setTarif,createVidjet,deleteVidjet,setColorRedux,setPriceName,setRegisterPath,setLoginPath,confEmail,
    confirePasswordChange
} = featureSlices.actions
export default featureSlices.reducer