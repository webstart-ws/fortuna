import React from "react";
import Footer from "../mainPage/footer/footer";
import QueryPrice from "../mainPage/queryPrice/queryPrice";


export default function Payment() {
    return (
        <section className="payment">
            <QueryPrice/>
            <Footer/>
        </section>
    )
}