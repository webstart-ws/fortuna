import React from "react";
import { useSelector } from "react-redux";
import Footer from "../mainPage/footer/footer";
import "./cooperation.css"
import "./responsiveCooperation.css"

export default function Cooperation() {
    const state = useSelector((state) => state.fortuna.partnerprogram)
    const statePay = useSelector((state) => state.fortuna.PayCount)

    return (
        <section className="cooperation">
            <div className="coopChild">
                <h2 className="titleCoop">գործընկերի ծրագիր</h2>
                <p className="help">
                    Օգնություն տեղեկանալ ծառայությունից այլ գործարարների եւ վաստակել
                    է 20% յուրաքանչյուր վճարման. Վճարման տոկոսը կավելանա, եթե կարողանաք ցույց տալ ծավալը
                 </p>
                {state.map((val) => {
                    return <div key={val.id} className="payCount">
                        <p>Գրանցումների-{val.registerCount}</p>
                        <p>Վճար-{val.paymentCount}</p>
                        <p>Հաշվեկշիռ - {val.BalanceCount} դրամ:</p>
                    </div>
                })}
                <button className="requirePayu">ՊԱՀԱՆՋԵԼ վՃԱՐՈՒՄ</button>
                <h2 className="payCLS">Վարձատրություն</h2>
                {statePay.map((val) => {
                    return <div  key={val.id} className="standartTarif">
                        <p className="standart">Սակագին ստանդարտ (ամիս) - {val.TariffstandardMount}դրամ</p>
                        <p className="standart">Սակագին ստանդարտ (ամիս) - {val.ExperttariffMount}դրամ</p>
                        <p className="standart">Սակագին ստանդարտ (ամիս) - {val.TariffstandardYear}դրամ</p>
                        <p className="standart">Սակագին ստանդարտ (ամիս) - {val.ExperttariffYear}դրամ</p>
                    </div>
                })}
            </div>
            <Footer/>
        </section>
    )
}