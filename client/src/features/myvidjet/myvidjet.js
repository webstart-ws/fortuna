import React from "react"
import { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Link } from "react-router-dom"
import { createVidjet, deleteVidjet, setTarif } from "../featuresSlices"
import Footer from "../mainPage/footer/footer"
import "./myvidjet.css"
import "./myVidjetResponsive.css"
import SettingWidjet from "./settings/settingsWidjet"


export default function Myvidjet() {
    const state = useSelector((state) => state.fortuna.vidjetProperty)
    const[openSetting,setOpenSetting ]= useState(true)
    const dispatch = useDispatch()

    return (
        <section className="myVidjet">
            <div className="widjet">
                <h2 className="title">Իմ վիդջեթները</h2>
                <div className="mainVid">
                {state.map((val) => {
                    return (
                        <div key={val.id} className="vidjetProperty">
                            <button className="vidjetButton vidjetButton2" onClick={() => {
                                dispatch(setTarif({id:val.id}))
                            }} style={{background: val.button ?  "#46B900" : "#AEAEAE"}}>
                                <p className={`vidjetButtonafter vidjetbuttonAfter2 ${val.button ? "vidjetLeft2" : ""}`}></p>
                            </button>
                            <p className="text">Նոր վիդջեթ</p>
                            <Link to="/onlyFortuna" className="iconLook"><img src={val.eyeImgsvg} alt="iconEye"/><span>բացել</span></Link>
                            <Link to="/aplication" className="iconLook"><img src={val.filtersvg} alt="iconEye"/><span>հայտեր</span></Link>
                            <button  onClick={() => setOpenSetting(!openSetting)} className="iconLook"><img src={val.toolssvg} alt="iconEye"/><span>կարգավորումներ</span></button>
                            <Link to="" className="iconLook"><img src={val.repeatsvg} alt="iconEye"/><span>կրկնօրինակել</span></Link>
                            <button onClick={() => {
                                dispatch(deleteVidjet({id:val.id}))
                            }} className="iconLook"><img src={val.deletsvg} alt="iconEye"/><span>ջնջել</span></button>
                        </div>
                    )
                })}
                </div>
                <button className="newVidjet" onClick={() => {
                    dispatch(createVidjet())
                }}>Նոր վիդջեթ</button>
            </div>
            <SettingWidjet open={openSetting} func={() => {
                setOpenSetting(true)
            }}/>
            <Footer/>
        </section>
    )
}