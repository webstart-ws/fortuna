import { useDispatch, useSelector } from "react-redux"
import React, { useState } from 'react';
import { SketchPicker } from 'react-color';
import { setColorRedux } from "../../featuresSlices";


export default function MainSettings() {
    const state = useSelector((state) => state.fortuna.mainSettings)
    const [color,setColor] = useState("black")
    const [colorMania,setcolorMania] = useState(false)
    const dispatch = useDispatch()


    return (
        <section className="mainSettings">
            {state.map((val,i) => {
                return  <div key={val.id}>
                    <div className="sliceInput"  onMouseLeave={() => setcolorMania(false)}>
                        <h2 className="titleSlice">{val.title}</h2>
                        { 
                            i === 0 ? <label className="colorLabel">
                                <input type="text" placeholder={color} onChange={(e) => setColor(e.target.value)} className={`inputSlice oneColorInput`}/>
                                <SketchPicker color={color}  className={`colorPicker ${colorMania ? "displayNone" : ""}`} onChangeComplete={(color) => {
                                    setColor(color.hex)
                                    dispatch(setColorRedux({color}))
                                }} style={{display:"none"}} />
                                <span className="circleColor" style={{background:color}} onClick={() => {
                                    setcolorMania(true)
                                }}></span>
                            </label> :
                            <input type="text" placeholder={val.value} className="inputSlice"/>
                        }
                    </div>
                    {
                        i === 1 ? <div className="centerP mainP"><span className="circleSpan">?</span><span className="textCirc">Նշեք, թե քանի վայրկյան անց widget պատուհանը կբացվի ինքնաբերաբար կամ թողնել դաշտը դատարկ</span></div> : 
                        i === 8 ? <div className="centerP mainP"><span className="circleSpan">?</span><span className="textCirc">Նշեք, թե քանի վայրկյան անց widget պատուհանը կբացվի ինքնաբերաբար կամ թողնել դաշտը դատարկ</span></div> 
                        : null
                    }
                </div>
            })}
        </section>
    )
}