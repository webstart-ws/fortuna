import React from "react"
import { useSelector } from "react-redux"




export default function Insert() {
    const state = useSelector((state) => state.fortuna.insertSettings)

    return (
        <section className="mainSettings">
            {state.map((val,i) => {
                return  <div key={val.id}>
                    <div className="sliceInput">
                        <h2 className="titleSlice">{val.title}</h2>
                        <div className="centerP"><span className="circleSpan">?</span><span className="textCirc">{val.description}</span></div>
                        { i === 2 ? 
                            <div className="btrix3">
                                <input type="text" placeholder={val.value} className="inputSlice btrixInput"/>
                                <button className="btrixButton">ԿԱՐԳԱՎՈՐԵԼ WEBHOOK-Ը</button>
                            </div> : null
                        }
                       { i === 2 ? <div className="centerP"><span className="circleSpan">?</span><span className="textCirc">{val.description2}</span></div> : null}
                        <input type="text" placeholder={ i === 2 ?  val.value2 : val.value} className="inputSlice"/>
                    </div>
                </div>
            })}
        </section>
    )
}