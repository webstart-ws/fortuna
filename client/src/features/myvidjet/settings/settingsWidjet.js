import React from "react"
import MainSettings from "./main"
import "./setings.css"
import "../myvidjet.css"
import "./responsivesettings.css"
import { useState } from "react"
import Bonus from "./bonus"
import Insert from "./insert"
import Code from "./code"
import { useDispatch } from "react-redux"
import { setPriceName } from "../../featuresSlices"


export default function SettingWidjet({open,func}) {
    const [url,SetUrl] = useState("/")
    const dispatch = useDispatch()
    const [value,setValue] = useState({ id:"",value:"" })
        
        
    


    return (
        <section className={`settings`} style={{display:open ? "none" : "flex"}}>
            <div className="settingsDiv">
                <button className="closeSettings" onClick={() => func()}></button>
                <div className="titleSet">
                    <h2>Կարգավորումներ</h2>
                    <div className="setingHeader">
                        <button onClick={() => SetUrl("/")} className={`navLink ${url === "/" ? "activeSettings" : "none"}`}>գլխավոր</button>
                        <button onClick={() => SetUrl("/bonus")} className={`navLink ${url === "/bonus" ? "activeSettings" : "none"}`}>բոնուսներ</button>
                        <button onClick={() => SetUrl("/insert")} className={`navLink ${url === "/insert" ? "activeSettings" : "none"}`}>ինտեգրում</button>
                        <button onClick={() => SetUrl("/code")} className={`navLink ${url === "/code" ? "activeSettings" : "none"}`}>կոդ</button>
                    </div>
                </div>
                {url === "/" ? <MainSettings/> : url === "/bonus" ? <Bonus func={(obj) => {
                    setValue(obj)
                }} value={value}/> : url === "/insert" ? <Insert/> : url === "/code" ?  <Code/> : "none"};

                <button className="buttonKeep" onClick={(e) => {
                    e.preventDefault()
                    dispatch(setPriceName({id:value.id,value:value.value}))
                    setValue({id:"",value:""})
                }}>Պահպանել</button>
            </div>
        </section>
    )
}