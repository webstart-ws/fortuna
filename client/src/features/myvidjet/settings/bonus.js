
import React from "react"
import { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { setPriceName } from "../../featuresSlices"


export default function Bonus({func,value}) {
    const state = useSelector((state) => state.fortuna.bonusSettings)
    const sectors = useSelector((state) => state.fortuna.fortunaPriceSlices)

    return (
        <section className="mainSettings">
            {state.map((val,i) => {
                return  <div key={val.id}>
                    { i === 0 ? <div className="centerP bonusP"><span className="circleSpan">?</span><span className="textCirc">Նշեք մեկից ութ բոնուսներ</span></div> : null }
                    <div className="sliceInput">
                        <h2 className="titleSlice">{val.title}</h2>
                        <input type="text" placeholder={sectors[i].label} value={val.id === value.id ? value.value : ""} onChange={(e) => func({
                            id:val.id,
                            value:e.target.value
                        })} className="inputSlice"/>
                        <label className="labelCheckSlice">
                            <input type="checkbox" className="checkBoxSlise"/>
                            մասնակցում է խաղարկությանը
                        </label>
                    </div>
                </div>
            })}
        </section>
    )
}