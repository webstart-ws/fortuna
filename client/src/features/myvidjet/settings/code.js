import React from "react"
import { useSelector } from "react-redux"



export default function Code() {
    const state = useSelector((state) => state.fortuna.codeSettings)


    return (
        <section className="mainSettings">

            {state.map((val,i) => {
                return  <div key={val.id}>
                    <div className="sliceInput">
                        <h2 className="titleSlice">{val.title}</h2>
                        <div className="centerP"><span className="circleSpan">?</span><span className="textCirc">{val.description}</span></div>
                        <textarea type="text" value={val.value} onChange={() => {
                            console.log("fsaf")
                        }} className="inputSliceTexterea"></textarea>
                    </div>
                </div>
            })}
        </section>
    )
}