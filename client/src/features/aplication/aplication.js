import { useDispatch, useSelector } from "react-redux"
import { Link } from "react-router-dom"
import React from "react"
import { createVidjet, deleteVidjet, setTarif } from "../featuresSlices"
import Footer from "../mainPage/footer/footer"
import "./aplication.css"
import "../myvidjet/myvidjet.css"
import "./responsiveaplication.css"



export default function Aplication() {
    const state = useSelector((state) => state.fortuna.aplication)

    return (
        <section className="myVidjet">
            <div className="appl">
                <h2 className="title">Հայտեր</h2>
                <div className="flexAppl">
                    <div className="headerApplication">
                        {state.header.map((val,i) => {
                            return <p key={i} className="headerATitle">{val}</p>
                        })}
                    </div>
                    {state.content.map((val) => {
                        return <div key={val.id} className="contentApp">
                                <p className="headerATitle">{val.count}</p>
                                <p className="headerATitle">{val.date}</p>
                                <p className="headerATitle">{val.Contact}</p>
                                <p className="headerATitle">{val.Bonus}</p>
                                <p className="headerATitle">{val.Url}</p>
                        </div>
                    })}
                </div>
                
            </div>

            <Footer/>
        </section>
    )
}