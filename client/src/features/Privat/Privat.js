import { useSelector } from "react-redux"
import Footer from "../mainPage/footer/footer"
import "./PrivatiPolis.css"
import "./responsPrivacy.css"


export default function Privat() {
    const state = useSelector((state) => state.fortuna.privaciPolic)
    return (
        <section className="PrivatiPolic">
            <div className="PrivacyDiv">
                {state.map((val,i) => {
                    return (
                        <div key={val.id} className="sepDiv">
                            <h2 className={`titlePrivacy ${i === 0 ? "oneTitle" : ""}`}>{val.title}</h2>
                            {val.text.map((valText) => {
                                return (
                                    <p  key={Math.random()} className="contentPolic">{valText}</p>
                                )
                            })}
                        </div>
                    )
                })}
            </div>
            <Footer/>
        </section>
    )
}