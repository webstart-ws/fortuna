import React, { useState } from "react"
import { Link } from "react-router-dom"
import "../register/register.css"
import "../responsiveRegister.css"
import "./forget.css"

import { useDispatch, useSelector } from "react-redux"
import { sendEmail } from "../asyncLogin"
import { confEmail } from "../../featuresSlices"


export default function ForgetPassword() {
    const [repeatPassword,setRepeat] = useState(false)
    const forgetPasswordAuth = useSelector((state) => state.fortuna.forgetPasswordAuth)
    const dispatch = useDispatch()

    // if(forgetPasswordAuth === "forgetFalse") {
    //     setTimeout(() => {dispatch(confEmail())},2000);
    // }
    console.log(forgetPasswordAuth,"forget");
    return (
        <section className="register">
            <h2 className="regTitle" style={{display:forgetPasswordAuth === "emailForgetTrue" ? "none" : ""}}>Մոռացել ես գաղտնաբառը</h2>
            <form className="formRegister" style={{display:forgetPasswordAuth === "emailForgetTrue" ? "none" : ""}} autoComplete="off" onSubmit={(e) => {
                e.preventDefault()
                const body = e.target;
                const pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
                if(pattern.test(body[0].value)) {
                    dispatch(sendEmail({email:body[0].value}));
                } else {
                    setRepeat(true);
                    setTimeout(() => {setRepeat(false)},2000);
                }
            }}>
                <label className="labelRegister">
                    <input name="email" placeholder="էլ. Փոստ"
                        className={`regInput ${repeatPassword ? "refuseBorder" : forgetPasswordAuth === "forgetFalse" ? "refuseBorder" : ""}`} 
                    />
                    {
                        repeatPassword ? <p className="refusedMessage">Այդպիսի էլ-փոստի հասցե գոյություն չունի</p> : 
                        forgetPasswordAuth === "forgetFalse" ? <p className="refusedMessage">էլ-փոստի հասցեն սխալ է</p> : 
                        forgetPasswordAuth === "emailForgetTrue" ? 
                        <p className="refusedMessage" style={{color:"green"}}>Ձեր էլ փոստին ուղարկվել է հաստատող նամակ</p> : ""
                    }
                </label>
                <button to="/newPassword" className="regLink">Առաջ</button>
            </form>
            <div className="confirmMessigeText" style={{display:forgetPasswordAuth === "emailForgetTrue" ? "" : "none"}}>
                <img className="messageImgBackTo" src="../../images/confiremMessage.svg"></img>
                <p className="textMessage">The letter has been sent to your E-mail</p>
                <Link to="/" onClick={() => {
                    dispatch(confEmail())
                }} className="backTomassage"><img src="../../images/backToSlack.svg"/>
                </Link>
            </div>
        </section>
    )
}


