import React from "react"
import { useState } from "react"
import { Link, Navigate,useLocation} from "react-router-dom"
import "./register.css"
import "../responsiveRegister.css"
import { useDispatch, useSelector } from "react-redux"
import { setRegisterPath } from "../../featuresSlices"
import { registerFetch } from "../asyncLogin"


export default function Register() {
    const location = useLocation()
    const dispatch = useDispatch()
    const [typeInput,setTypeInput] = useState({
        password:true,
        repeatPassword:true
    })
    console.log(registerIsGoodAuth,repeatPassword);

    const registerIsGoodAuth = useSelector((state) => state.fortuna.registerIsGoodAuth)
    const [repeatPassword,setRepeat] = useState(true)

    if(registerIsGoodAuth) {
        setTimeout(() => {dispatch(setRegisterPath())},100)
        return <Navigate to="/login" replace={true}/>
    } 

    if(registerIsGoodAuth === null) {
        setTimeout(() => {dispatch(setRegisterPath())},2000)
    } 
   

    return (
        <section className="register">
            <h2 className="regTitle">Գրանցվել</h2>
            <form className="formRegister" autoComplete="off" onSubmit={(e) => {
                e.preventDefault();
                const body = e.target;
                let pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

                if(body[1].value === body[3].value && pattern.test(body[0].value) && body[1].value !== "" && body[5].checked) {
                    dispatch(registerFetch({
                        email:body[0].value,
                        password:body[1].value
                    }));
                } else if(!pattern.test(body[0].value)) {
                    setRepeat(null);
                    setTimeout(() => {setRepeat(true)},3000);
                } else if(body[1].value.length < 6) {
                    setRepeat("NaN");
                    setTimeout(() => {setRepeat(true)},3000);
                } else if(!(body[1].value === body[3].value)) {
                    setRepeat(false);
                    setTimeout(() => {setRepeat(true)},3000); 
                } else if(!body[5].checked) {
                    setRepeat("lidoGenFalse");
                    setTimeout(() => {setRepeat(true)},3000);
                }
            }}>
                <label className="labelRegister">
                    <input name="email" title="write email" className={`regInput 
                    ${registerIsGoodAuth === null ? "refuseBorder" : "" } 
                    ${repeatPassword === null ? "refuseBorder" : "" }`} placeholder="էլ. Փոստ" />
                    {
                        registerIsGoodAuth === null ? <p className="refusedMessage">Այս էլփոստի հասցեն արդեն գրանցված է</p> : 
                        repeatPassword === null ? <p className="refusedMessage">Այդպիսի էլփոստի հասցե գոյություն չունի</p> : ""
                    }
                </label>
                <label className="labelRegister">
                    <input type={typeInput.password ? "password" : "text"} title="write password" name="password" 
                    className={`regInput ${repeatPassword === "NaN" ? "refuseBorder" : "" }`}  placeholder="Գաղտնաբառ"/>
                    {repeatPassword === "NaN" ? <p className="refusedMessage">ծածկագիրը պետք է լինի առնվազն 6 նիշ</p> : ""}
                    <button type="button" onClick={() => setTypeInput({
                        ...typeInput,password:!typeInput.password
                    })}><img src="../../images/eyereg.svg" alt="passwordEye"/></button>
                </label>
                <label className="labelRegister">
                    <input type={typeInput.repeatPassword ? "password" : "text"} name="repeatPassword"  title="repeate password"
                        className={`regInput ${repeatPassword === false ? "refuseBorder" : "" }`} placeholder="Կրկնել գաղտնաբառը"
                    />
                    <button type="button" onClick={() => setTypeInput({
                        ...typeInput,repeatPassword:!typeInput.repeatPassword
                    })}><img src="../../images/eyereg.svg" alt="passwordEye"/></button>
                </label>
                <div className="lidoGenLabel">
                    <label className="lidoLabel">
                        <input type="checkbox" name="GeneretionCheckbox" 
                        className={`checkbox ${ repeatPassword === "lidoGenFalse" ? "refuseBorder" : ""}`} />
                         <p className="ligoText" 
                            style={{color:repeatPassword === "lidoGenFalse" ? "red" : ""}}
                         >Ես ուզում եմ օգտակար նյութեր ստանալ լիդոգեներացիայի եւ թարմացումների մասին</p>
                    </label>
                </div>
                <button className="regLink">Գրանցվել</button>
                <div className="exastigAcount">Արդեն ունեք հաշիվ: <Link to="/login" className="AcountLink">Մուտք գործեք</Link></div>
            </form>
        </section>
    )
}
