import React, { useEffect } from "react"
import { useState } from "react"
import { Link, useLocation, useNavigate } from "react-router-dom"
import "./newPassword.css"
import "../register/register.css"
import "../responsiveRegister.css"
import { useParams } from "react-router-dom"
import { useDispatch, useSelector } from "react-redux"
import { checkExpiration, createNewPassword } from "../asyncLogin"
import { confirePasswordChange } from "../../featuresSlices"



export default function NewPassword() {
    const passwordChanged_done = useSelector((state) => state.fortuna.passwordChanged_done)
    const dispatch = useDispatch()
    const params = useParams()
    const [typeInput,setTypeInput] = useState({
        password:true,
        repeatPassword:true
    })
    useEffect(() => { dispatch(checkExpiration(params.token)) })
    return (
        <section className="register">
            <h2 className="regTitle">Նոր գաղտնաբառ</h2>

            {   passwordChanged_done.changed ? 
                <form className="formRegister" style={{display:passwordChanged_done.done ? "none" : "flex"}} autoComplete="off" onSubmit={(e) => {
                    e.preventDefault()
                    let body = e.target
                    if(body[0].value === body[2].value && body[0].value !== undefined && body[0].value.length > 5) {
                        dispatch(createNewPassword({
                            newPassword:body[0].value,
                            compireToken:params.token
                        }))
                    } 
                }}>
                    <label className="labelRegister">
                        <input type={typeInput.password ? "password" : "text"} name="password" className="regInput" placeholder="Գաղտնաբառ"/>
                        <button type="button" onClick={() => setTypeInput({
                            ...typeInput,password:!typeInput.password
                        })}><img src="../../images/eyereg.svg" alt="passwordEye"/></button>
                    </label>
                    <label className="labelRegister">
                        <input type={typeInput.repeatPassword ? "password" : "text"} name="repeatPassword"  className="regInput" placeholder="Կրկնել գաղտնաբառը"/>
                        <button type="button" onClick={() => setTypeInput({
                            ...typeInput,repeatPassword:!typeInput.repeatPassword
                        })}><img src="../../images/eyereg.svg" alt="passwordEye"/></button>
                    </label>
                    <button className="regLink">Առաջ</button>
                </form> : 

                <div className="confirmMessigeText" style={{display:passwordChanged_done.done ? "flex" : "none"}}>
                    {
                        passwordChanged_done.message === "Password Has changed" ? 
                        <img className="messageImgBackTo" src="../../images/thumb.svg"></img> : 
                        <img className="messageImgBackTo" src="../../images/errorSvg.png"></img>
                    }
                    <p className="textMessage">{passwordChanged_done.message}</p>
                    <Link to="/" onClick={() => {
                        dispatch(confirePasswordChange())
                    }} className="backTomassage"><img src="../../images/backToSlack.svg"/>
                    </Link>
                </div>
            }
        </section>
    )
}