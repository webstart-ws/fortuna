import React, { useEffect } from "react"
import { useState } from "react"
import { Link, Navigate } from "react-router-dom"
import "../register/register.css"
import "./login.css"
import "../responsiveRegister.css"
import { setLoginPath } from "../../featuresSlices"
import { useDispatch, useSelector } from "react-redux"
import { faceBookLogin, loginFetch } from "../asyncLogin"
import Backdrop from '@mui/material/Backdrop';
import CircularProgress from '@mui/material/CircularProgress';




export default function Login() {
    const [typeInput,setTypeInput] = useState(true)
    const dispatch = useDispatch()
    const loginIsGoodAuth = useSelector((state) => state.fortuna.loginIsGoodAuth)


    if(loginIsGoodAuth === "Good") {
        setTimeout(() => {dispatch(setLoginPath())},100)
        return <Navigate to="/myVidjet" replace={true}/>
    } else if(loginIsGoodAuth === "falseEmail") {
        setTimeout(() => {dispatch(setLoginPath())},2000)
    } else if(loginIsGoodAuth === "falsePassword") {
        setTimeout(() => {dispatch(setLoginPath())},2000)
    } 


    if(loginIsGoodAuth === "loading") {
      return <div>
          <Backdrop sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}  open={true}>
            <CircularProgress color="inherit" />
          </Backdrop>
      </div>
    } 

    else {
        return (
            <section className="register">
                <h2 className="regTitle">Բարի գալուստ</h2>
                <form className="formRegister" autoComplete="off" onSubmit={(e) => {
                    e.preventDefault();
                    const body = e.target;
                    dispatch(loginFetch({ email:body[0].value, password:body[1].value }));
                }}>
                    <label className="labelRegister">
                        <input type="email" name="email" className={`regInput ${loginIsGoodAuth === "falseEmail" ? "refuseBorder" : ""}`} placeholder="էլ. Փոստ" required/>
                        {loginIsGoodAuth === "falseEmail" ? <p className="refusedMessage">Այս էլփոստի հասցեն գրանցված չէ</p> : ""}
                    </label>
                    <label className="labelRegister">
                        <input type={typeInput ? "password" : "text"} name="password" 
                            className={`regInput ${loginIsGoodAuth === "falsePassword" ? "refuseBorder" : ""}`}
                            placeholder="Գաղտնաբառ" required
                        />
                        {loginIsGoodAuth === "falsePassword" ? <p className="refusedMessage"> ծածկագիրը սխալ է</p> : ""}
                        <button type="button" onClick={() => setTypeInput(!typeInput)}><img src="../../images/eyereg.svg" alt="passwordEye"/></button>
                    </label>
                    <Link  to="/forgetPassword" className="forgetPassword">Մոռացել ես գաղտնաբառը?</Link>
                    <button className="regLink">Մուտք</button>
                    <div className="exastigAcount">Եթե չունեք հաշիվ։ <Link to="/register" className="AcountLink">Գրանցվեք</Link></div>
                    <button className="socialLogin" onClick={(e) => { e.preventDefault();
                        dispatch(faceBookLogin()) 
                    }}>Facebook</button>
                    <button className="socialLogin" style={{backgroundColor:"red"}} onClick={(e) => { 
                        e.preventDefault();
                        window.open("http://localhost:5000/auth/google","_self") 
                    }}>Google</button>
                </form>
            </section>
        )
    }


    
    
}