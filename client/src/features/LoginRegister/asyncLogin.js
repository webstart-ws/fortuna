import { createAsyncThunk } from "@reduxjs/toolkit";

export const registerFetch = createAsyncThunk(
    "fortuna/fetchRegister",
    async (body) => {
        const response = await fetch("/register",{
            method:"post",
            headers:{"content-type" : "application/json"},
            body:JSON.stringify(body)
        })
        return response.json()
    }
)

export const loginFetch = createAsyncThunk(
    "fortuna/fetchLogin",
    async (body) => {
        // console.log(body);
        const response = await fetch("/login",{
            method:"post",
            headers:{"content-type" : "application/json"},
            body:JSON.stringify(body),
        })
        return response.json()
    }
)

export const sendEmail = createAsyncThunk(
    "fortuna/sendEmail",
    async (body) => {
        // console.log(body);
        const response = await fetch("/forgetPassword",{
            method:"post",
            headers:{"content-type" : "application/json"},
            body:JSON.stringify(body),
        })
        return response.json()
    }
)

export const logOuth = createAsyncThunk(
    "fortuna/logOuth",
    async () => {
        const response = await fetch("/logOuth")
        return response.json()
    }
)

export const resetPassword = createAsyncThunk(
    "fortuna/resetPassword",
    async (body) => {
        const response = await fetch("/resetPassword",{
            method:"POST",
            headers:{"content-type" : "application/json","accept" : "application/json"},
            body:JSON.stringify(body),
        })
        return response.json()
    }
)

export const createNewPassword = createAsyncThunk(
    "fortuna/createNewPassword",
    async (body) => {
        const response = await fetch("/createNewPassword",{
            method:"POST",
            headers:{"content-type" : "application/json","accept" : "application/json"},
            body:JSON.stringify(body),
        })
        return response.json()
    }
)

export const checkExpiration = createAsyncThunk(
    "fortuna/checkExpiration",
    async (token) => {
        const response = await fetch(`/checkExpiration/${token}`,{
            method:"GET", headers:{"content-type" : "application/json","accept" : "application/json"},
        })
        return response.json()
    }
)

export const faceBookLogin = createAsyncThunk(
    "fortuna/faceBookLogin",
    async (token) => {
        const response = await fetch(`/oauth/facebook/`,{
            cors:'cors',
            method:"GET", 
            headers:{"content-type" : "application/json","accept" : "application/json"},
        })
        return response.json()
    }
)

export const authenticationToken = createAsyncThunk(
    "fortuna/authenticationToken",
    async (token) => {
        const response = await fetch(`/login/success`,{
            method:"GET",
            credentials:"include",
            headers:{
                accept:"application/json","Content-Type" : "application/json","Access-Control-Allow-Credentials" : true,
            }
        })
        return response.json()
    }
)

