import React from "react"
import { useSelector } from "react-redux"
import { Link } from "react-router-dom"
import "./footer.css"
import "./footerResponsive.css"


export default function Footer() {
    const state = useSelector((state) => state.fortuna.footerImg)
    return (
        <section className="footer">
            <p className="logo">LOGO</p>
            <div className="reklamImg">
                {state.map((val,i) => <img key={i} src={val}/>)}
            </div>
            <div className="footOption"><Link className="footerLink" to="/PrivatiPolic">Գաղտնիության քաղաքականություն</Link> 2022</div>
        </section>
    )
}