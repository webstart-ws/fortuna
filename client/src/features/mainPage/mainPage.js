import React from "react"
import "./mainPage.css"
import "./mainPAgeResponsive.css"

import { Link } from "react-router-dom"
import Fortuna from "./fortuna/fortuna"
import Notice from "./noteSection/noteSection"
import QueryPrice from "./queryPrice/queryPrice"
import Footer from "./footer/footer"

export default function MainPage() {
    return (
        <section className="mainPage">
            <h1 className="nameAndDescription">Վիջեթ "Ֆորտունայի անիվ" կայքի համար</h1>
            <h2 className="descriptionTry">Ֆորտունայի անիվ մեծացնում է դարձի է հայտի եւ պահպանում է հեռացող այցելուներին. Փորձեք այն անվճար!</h2>
            <Link to="/register" className="tryButton">Փորցել</Link>
            <Fortuna/>
            <Notice/>
            <QueryPrice/>
            <Footer/>
        </section>
    )
}