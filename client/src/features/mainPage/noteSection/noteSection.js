import React from "react"
import { useDispatch, useSelector } from "react-redux"
import { openDescripotion } from "../../featuresSlices"
import "./noteSection.css"
import "./responsiveNote.css"

export default function Notice() {
    const state = useSelector((state) => state.fortuna.genAplication)
    const guidState = useSelector((state) => state.fortuna.GuidforPopUp)
    const dispatch = useDispatch()


    return (
        <section className="noteMain">
            <h2 className="noticeTitle">Անիվը հեշտ է օգտագործել տարբեր խնդիրների համար, օրինակ:</h2>

            <div className="genAplication">
                {state.map((val) => {
                    return (
                        <div key={val.id} className="descDiv">
                            <div className="icon" >
                                <img className="innerIcon" src={val.src} alt="icon"/>
                            </div>
                            <h2 className="titleDescGen">{val.title}</h2>
                            <p className="descicon">{val.text}</p>
                            <p className="underLineinGen" style={{background:val.color}}></p>
                        </div>
                    )
                })}
            </div>

            <div className="NotificationDiv">
                <h2 className="notificatioTitle">Անիվը հեշտ է օգտագործել տարբեր խնդիրների համար, օրինակ:</h2>
                {guidState.map((val) => {
                    return (
                        <div key={val.id} className={`notDiv ${val.id === 1 ? "newNot" : ""}`} onClick={() => {
                            dispatch(openDescripotion({id:val.id}))
                        }}>
                            <div className="mainTestDiv">
                                <h2 className="titleTest">{val.title}</h2>
                                <div>
                                    {val.open ? <p className="minus"></p> : <p className="plus"></p>}
                                </div>
                            </div>
                            <p className="minusDescription" style={{display:val.open ? "block" : "none"}}>{val.description}</p>
                        </div>
                    )
                })}
            </div>
        </section>
    )
}