import React from "react"
import { useState } from "react"
import { useSelector } from "react-redux"
import { Link } from "react-router-dom"
import "./queryPrice.css"
import "./responsivequery.css"




export default function QueryPrice() {
    const state = useSelector((state) => state.fortuna.TarifPrice)
    const [tarif,setTarif] = useState(false)

    return (
        <section className="mainQueriPrice">
            <h2 className="titlePriceQuery">Հարցի գինը</h2>
            <p className="descriptionQuery">Գործիք, որը ուղղակիորեն ազդում է փողի վրա</p>
            <div className="vidjet">
                <p className="price">Մեկ ամսվա համար</p>
                <button className="vidjetButton" onClick={() => {
                    setTarif(!tarif)
                }} style={{background: tarif ?  "#46B900" : "#AEAEAE"}}>
                    <p className={`vidjetButtonafter  ${tarif ? "vidjetLeft" : ""} `}></p>
                </button>
                <p className="price">մեկ տարվա համար</p>
            </div>

            <div className="tarifValue">
                {state.map((val) => {
                    return (
                        <Link to="/payment" key={val.id} className="sizeTarifChild">
                            <p className="tarifCount">{tarif ? val.yearPrice : val.price}<span>֏</span></p>
                            <p className="titelCount">Test Drive</p>
                            <p className="borderColorPrice" style={{background:val.borderColor}}></p>
                            <p className="formForm">{val.form} հայտ</p>
                            <p className="formVidjet">{val.vidjet} վիջեթ</p>
                        </Link>  
                    )
                })}
            </div>
                
        </section>
    )
}