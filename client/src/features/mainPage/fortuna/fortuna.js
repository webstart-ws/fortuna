import React from "react"
import "./fortuna.css"
import "./fortunaresponsive.css"

import { useDispatch, useSelector } from "react-redux"
import { useEffect, useRef, useState } from "react"
import axios from "axios"
import countryTelData from 'country-telephone-data'
import { fetchAsync } from "../../featuresSlices"


export default function Fortuna() {
    const state = useSelector((state) => state.fortuna.fortunaCircleSlice)
    const sectors = useSelector((state) => state.fortuna.fortunaPriceSlices)
    const canvasRef = useRef(null)
    const canvasDivRef = useRef(null)
    const dispatch = useDispatch()


    const [languig,setTelCanal] = useState(true)
    const [openLanguig,setOpenLanguig] = useState(false)
    const [turn,setTurn] = useState(0)
    const [valueTel,setValueTel] = useState("")
    const [ruletkaNumber,setRuletkaNumber] = useState("")
    const [openError,SetopenError] = useState(false)
    const [timeOut,setTime] = useState(true)
    const canvasDeminision = window.innerWidth
    // console.log(ruletkaNumber)


    function font() {
        if(canvasDeminision <= 1700 && canvasDeminision > 1024) {
            return  "13px"
        } else if(canvasDeminision <= 1024 && canvasDeminision > 650) {
            return "9px"
        } else if(canvasDeminision <= 650) {
            return  "5px" 
        } else {
            return "20px"
        }
    }

    function textCordinate() {
        if(canvasDeminision <= 1700 && canvasDeminision > 1024) {
            return  70
        } else if(canvasDeminision <= 1024 && canvasDeminision > 650) {
            return 40
        } else if(canvasDeminision <= 650) {
            return  15
        } else {
            return 100
        }
    }

    function draw(context,canvas,i,startPoint,ArcPoint,val,arc,rad) {
        startPoint += i * 0.25
        ArcPoint += i * 0.25
        const ang = arc * i;
        context.save();
        // sectors
        context.beginPath();
        context.fillStyle = val.color;
        context.moveTo(canvas.width / 2, canvas.width / 2);
        context.arc(canvas.width / 2, canvas.width / 2, canvas.width / 2 - 16,startPoint * Math.PI, ArcPoint * Math.PI);
        context.lineTo(canvas.width / 2, canvas.width / 2);
        context.fill();
        // text
        context.translate(canvas.width / 2, canvas.width / 2);
        context.rotate(ang + arc / 2);
        context.font = `italic  ${font()} serif `;
        context.fillStyle = "white";
        context.fillText(sectors[i].label,textCordinate(),3);
        // 
        context.restore();
    }

    useEffect(() => {
        const canvas = canvasRef.current;
        const context = canvas.getContext("2d");
        const divForCanvas = canvasDivRef.current
        const divForCanvasWidth = window.getComputedStyle(divForCanvas).getPropertyValue("width")
        const numberforCanvasWidth = divForCanvasWidth.replace(/[^0-9\.]+/g,'')

        const divForCanvasHeight = window.getComputedStyle(divForCanvas).getPropertyValue("height")
        const numberforCanvasHeight = divForCanvasHeight.replace(/[^0-9\.]+/g,'')
        canvas.width = +numberforCanvasWidth 
        canvas.height = +numberforCanvasHeight  

        if(canvasDeminision <= 1440 && canvasDeminision > 768) {
            canvas.width += 10
            canvas.height += 10
        } else if(canvasDeminision <= 768 && canvasDeminision > 375) {
            canvas.width += 20
            canvas.height += 20
        } else if(canvasDeminision <= 375) {
            canvas.width += 25
            canvas.height += 25
        }

        let startPoint = 0;
        let ArcPoint = 0.25;
        const PI = Math.PI;
        const TAU = 2 * PI;
        const arc = TAU / sectors.length;
        const rad = canvas.width / 2;
        // getIndex
        context.clearRect(0, 0, canvas.width, canvas.height);
        sectors.forEach((val,i) => draw(context,canvas,i,startPoint,ArcPoint,val,arc,rad));
    },[])

    useEffect(() => {
        const AA = Math.floor(turn / 360)
        const BB = AA * 360
        let degree = turn - BB
        if(degree === 45 || degree === 90 || degree === 135 || degree === 180 || degree === 225 || degree === 270 || degree === 315 || degree === 360 ) {
            degree++
        }
        let round = Math.floor(degree / 45)
        let rulNum = sectors.filter((val) => val.id === round)
        setRuletkaNumber(rulNum[0].label)
        // alert Result
        // alert(JSON.stringify(rulNum))
    },[turn])
    return (
        <div className="mainFortuna" >
            <div className="divForCanvas" ref={canvasDivRef}>
                <canvas width="588px" height="588px" className="canvas" ref={canvasRef} style={{transform: `rotate(${turn}deg)`}}></canvas>
                <span className="centrCircle"></span>
                <span className="slackChoose"></span>
                <div className="divForCanvas secondCanvasPatend" style={{ transform: `rotate(${turn}deg` }}>
                {state.map((val,i) => <p key={val.id}className="fortunaSlices" style={{backgroundColor:val.color,right:val.positionRight,top:val.positionTop}}></p>)}

                </div>
            </div>
            <form className="inputField" onSubmit={(e) => {
                    e.preventDefault()
                    let pattern = /[1-9]{2}[0-9]{3}[0-9]{3}/g;
                    let result = pattern.test(valueTel);
                    if(result === true && timeOut === true) { 
                        setTime(false)
                        setTimeout(() => {
                            setTime(true)
                        },5100)
                        // axios.post("inputField",{inputTel:valueTel })
                        // .then((response) => {
                        //     if(response) { 
                        //         setTurn(Math.floor((Math.random() * 50000) + 10000))
                        //         SetopenError(false)
                        //     }
                        // })

                        // jamanakavor 
                        SetopenError(false) 
                        setTurn(Math.floor((Math.random() * 50000) + 10000))
                    }  
                    else {SetopenError(true) }
                    setTimeout(() => {
                        SetopenError(false) 
                    },5100)
            }}> 
                <input id="ruletkaValue" value={ruletkaNumber} name="ruletkaNumber" readOnly/> 
                <h2 className="descriptionInput">Շրջադարձ անիվը!</h2>
                <p className="sendTelefon">Մուտքագրեք Ձեր հեռախոսահամարը հաղթելու մրցանակը!</p>
                <label className="inpuitLabel">
                    <input type="tel" style={{border: `1.25803px solid ${openError ? "#FF0000" : "" }`}} className="inputTel" name="telefone" placeholder="Մուտքագրեք հեռախոսահամարը" value={valueTel}  onChange={(e) => {
                        setValueTel(e.target.value)
                    }} />
                    <div className="flagField" onClick={() => setOpenLanguig(!openLanguig)}>
                        <input type="text" value={languig === "armenia" ? "armenia" : "anglish"} name="languig" style={{display:"none"}} readOnly/>
                        <div className={`imgState ${openLanguig ? "changeSize" : ""}`}>
                            <img src="../../images/flagField.png" onClick={() => setTelCanal(true)} style={{order:languig ? "0" : "1"}}/>
                            <img src="../../images/anglish.svg" onClick={() => setTelCanal(false)} />
                        </div>
                        <img src="../../images/slack.svg" className="slack" />
                    </div>
                </label>
                <p className="messageTEl" style={{display:openError ? "block" : "none"}}>հեռախոսահամարը սխալ է</p>
                <button className={`turnToButton ${ !openError ? "turnToButtonTwo" : ""}` }>Շրջադարձ</button>
            </form>
        </div>
    )
}