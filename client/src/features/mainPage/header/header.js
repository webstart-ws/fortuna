import React from "react"
import { useEffect, useState } from "react"
import { useDispatch } from "react-redux"
import { Link, useLocation, useNavigate } from "react-router-dom"
import { logOuth } from "../../LoginRegister/asyncLogin"
import "./header.css"
import "./responsiveHeader.css"


export default function Header({path,pathNameAnother,pathforMenyu}) {
    const [hover,setHover] = useState("")
    const [openMenyu,setopenMenyu] = useState(false)
    const history = useLocation()
    useEffect(() => { setopenMenyu(false) },[history.pathname])
    const navigate = useNavigate()
    const dispatch = useDispatch()

    
    
   
    // s%3Aj4m5eflhrP1bUHNKAGE_yjeGQI4d2M_f.l6lhEjbJeU%2FOgwGCDHz6IyIyDfSmUxlnM8P9nJgHW%2Fc

    return (
        <section className="header">
            <Link to="/" className="logo">LOGO</Link>
            <div className={`headerforVidjet ${openMenyu ? "forNav" : ""}`} style={{display:pathNameAnother ? "flex" : "none"}}>
                <Link to="/myVidjet" className="anotherGeader">Վիդջեթներ</Link>
                <Link to="/payment" className="anotherGeader">Վճարում</Link>
                <Link to="/cooperation" className="anotherGeader">Համագործակցություն</Link>
                <button onClick={async () => {
                    dispatch(logOuth())
                    navigate("/")
                }} className="anotherGeader">Դուրս գալ</button>
            </div>
            {
               !pathforMenyu ? <button className={`substitutMenu ${openMenyu ? "substitutMenuXButton" : ""}`} onClick={() => {
                setopenMenyu(!openMenyu)
                }}></button> : null
            }
            <nav className={`nav ${openMenyu ? "forNav" : ""}`} style={{display:path ? "none" : "flex"}}>
                <Link to="/login" className={`linkLoginRegister linkLogin ${hover}`}>Մուտք</Link>
                <Link to="/register" className="linkLoginRegister linkegister" 
                    onMouseEnter={() => setHover("linkHoverColor")}
                    onMouseLeave={() => setHover("")}
                >
                    Գրանցվել
                </Link>
            </nav>
        </section>
    )
}

// className={`nav ${openMenyu ? "forNav" : ""}`}