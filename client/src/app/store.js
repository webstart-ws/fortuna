import { configureStore } from '@reduxjs/toolkit';
import fortunaReducer from "../features/featuresSlices"

export const store = configureStore({
  reducer: {
    fortuna: fortunaReducer,
  },
});
