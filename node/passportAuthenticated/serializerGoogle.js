import GoogleStrategy from "passport-google-oauth20"
export function getPasspotConfig_By_Google(passport,pool,app) {
  passport.use(new GoogleStrategy({
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      callbackURL: "/auth/google/callback",
      passReqToCallback:true
    },async (request, accessToken, refreshToken, profile, done) =>  {
      const user = await pool.query("SELECT * FROM users where email = ?",[profile.emails[0].value])
      return done(null, user[0][0]);
    }
  ));

  app.get("/auth/google",passport.authenticate("google",{ scope:["email","profile"]}))
  app.get("/auth/google/callback",passport.authenticate("google",{
    successRedirect: "http://localhost:5000/login",
    failureRedirect: "/login/failed"
  }))

}



