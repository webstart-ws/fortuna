import passportLocal from "passport-local"
import bcrypt from "bcrypt"
import { Serialazer } from "./serializer.js"

function passpotConfig(passport,pool) {
    passport.use(new passportLocal.Strategy({
        usernameField:"email"
    },async (email,password,done) => {
        const user = await pool.query("SELECT * FROM users where email = ?",[email])
        if(!user[0].length) {
            return done(null,null,{message:"falseEmail"})
        }
    
        if(await bcrypt.compare(password,user[0][0].password)) {
            return done(null,user[0][0]);
        }
    
        done(null,null,{message:"falsePassword"})
    }))

    Serialazer(passport,pool)
}


function checkAuthenticated(req,res,user,info,error) {
    if (req.isAuthenticated()) {
        console.log("authenticated Login succes");
        return res.send(user)
    } else {
        console.log("authenticated Login failed");
        return res.send(info)
    }
}

export { passpotConfig,checkAuthenticated }
   
    
