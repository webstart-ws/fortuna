import nodemailer from "nodemailer"
import path from "path"
import fs from "fs"
import { getTemplate } from "./MailTemplate.js"
const filePath = path.resolve()
const currenthFile = path.join(filePath,"/mailTo/mailto.txt")

// async..await is not allowed in global scope, must use a wrapper
async function mailTo(email,newToken) {

  let transporter = nodemailer.createTransport({
    host: process.env.MAIL_HOST,
    port: process.env.MAIL_PORT,
    secure: false, // true for 465, false for other ports
    auth: {
      user: process.env.MAIL_USERNAME, // generated ethereal user
      pass:  process.env.MAIL_PASSWORD, // generated ethereal password
    },
    tls: { rejectUnauthorized: false }
  });

  let info = await transporter.sendMail({
    from: `Fortuna Company "<torgomyandavid96@gmail.com>"`, // sender address
    to:email, // list of receivers
    subject: "Confirem Password", // Subject line
    text: "Hello world?", // plain text body
    html: getTemplate(newToken,email) // plain html body
    // attachments:[{filename:"./anglish.png"}]
      
    

    // davit.torgomyan96@mail.ru
    // html: await fs.promises.readFile(currenthFile,"utf-8") // plain html body
  });

  console.log("Message sent: %s", info.envelope);
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
}
export { mailTo }