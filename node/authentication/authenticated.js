export function authenticatedLogin(app) {
    app.get("/login/failed",(req,res) => {
        res.status(401).json({
            success:false,
            message:"there are no authorized user"
        })
    })

    app.get("/login/success",(req,res) => {
        console.log(req.isAuthenticated(),"google auth true");
        if(req.user && req.isAuthenticated()) {
            res.status(200).json({
            success:true,
            message:"user is authorized",
            user:req.user,
            // cookies:req.cookies
            })
        } else {
            res.status(401).json({
            success:false,
            message:"failure"
            })
        }
    })
}