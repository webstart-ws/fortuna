import express from "express"
import { createPool } from "mysql2/promise"
import session from "express-session"
import cors from "cors"
import path from "path"
import passport  from "passport"
import bcrypt from "bcrypt"
import jsonwebtoken from "jsonwebtoken"
import { mailTo } from "./node/mailTo/mailTo.js"
import { checkAuthenticated } from "./node/passportAuthenticated/passportConfig.js"
import { passpotConfig } from "./node/passportAuthenticated/passportConfig.js"
import { getPasspotConfig_By_Facebook } from "./node/passportAuthenticated/serialazerFacebook.js"
import { getPasspotConfig_By_Google } from "./node/passportAuthenticated/serializerGoogle.js"
import { Middleware } from "./node/middlewarejs/Middleware.js"
import { authenticatedLogin } from "./node/authentication/authenticated.js"

const __dirname = path.resolve()
const currenthFile = path.join(__dirname,"./client/build")
console.log(currenthFile);

const pool = createPool({
    host:process.env.db_host,
    user:process.env.db_user,
    password:process.env.db_password,
    database:process.env.db_name
});

const app = express()

app.use(session({
    secret:process.env.SECRET,
    // cookie: { expires : new Date(Date.now() + 3600000) },
    // cookie:{maxAge:60000},
    saveUninitialized:false,
    resave:false
}))
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(passport.initialize())
app.use(passport.session())
// app.use(Middleware)

app.use(cors({
    origin:"http://localhost:3000",
    methods:"GET,POST,PUT,DELETE",
    credentials:true
}))

// static
app.use(express.static("./client/build"))

// passport Authenticated midlware
passpotConfig(passport,pool)
getPasspotConfig_By_Facebook(passport,pool,app)
getPasspotConfig_By_Google(passport,pool,app)

// get request
app.get("/logOuth",(req,res) => {
    console.log("logauth is  -> " + req.isAuthenticated());
    req.logout();
    console.log("logauth is  -> " + req.isAuthenticated());
    res.send({success:true,message:"դուք հաջողությամբ դուրս եկաք հաշվից"});
})

app.get("/checkExpiration/:token",(req,res) => {
    try {
        const verify = jsonwebtoken.verify(req.params.token,process.env.ACCESTOKEN) 
        res.send({message:"everything is normal",done:false,changed:true})
    }
    catch(err) { 
        res.send({...err,done:true,changed:false})
    }
})

authenticatedLogin(app)

// midlware for Refresh
app.get('*',Middleware,(req, res) => {
    res.sendFile(path.resolve(currenthFile,"index.html"));
});

// post Request
app.post("/login",(req,res,next) => {
    passport.authenticate("local",{failureFlash: true},(err,user,info) => {
        req.logIn(user,function(error) {checkAuthenticated(req,res,user,info,error)});
    })(req,res,next);
})

app.post("/register",async (req,res) => {
    const {email,password} = req.body
    const bcryptPassword = await bcrypt.hash(password,10);
    const created_at = new Date().toLocaleString() 
    const body = {email,password:bcryptPassword,created_at:created_at}
    const response = await pool.query("SELECT * FROM users where email = ?",[email])
    if(response[0].length === 0){await pool.query(`INSERT INTO users SET ?`,body); res.send(response)}
    else res.send("refused")
})

app.post("/forgetPassword",async (req,res) => {
    const {email} = req.body
    const user = await pool.query("SELECT email, id  FROM users where email = ?",[email])
    if(user[0].length === 0){res.send({message:"forgetFalse"})}
    else {
        const newToken = jsonwebtoken.sign(user[0][0],process.env.ACCESTOKEN,{expiresIn:'300s'});
        req.session.jwt = newToken

        mailTo(email,newToken)
        res.status(200).send({message:"emailForgetTrue"})
    };
})

app.post("/createNewPassword",async (req,res) => {
    let verify = undefined;
    const {compireToken,newPassword} = req.body
    try {
        verify = jsonwebtoken.verify(compireToken,process.env.ACCESTOKEN) 
    }
    catch(err) { 
        res.send({...err,done:true})
    }
    if(!!verify && !!req.session.jwt === true) {
        const DateNow = new Date().toLocaleString();
        const resetPasswordEncoded = await bcrypt.hash(newPassword,10);
        await pool.query(`UPDATE users SET password = ?, changing_password_time = ? WHERE users.id = ?`,[resetPasswordEncoded,DateNow,verify.id]);
        req.session.jwt = undefined
        res.send({message:"Password Has changed",done:true,changed:false});
    } 
    else if(!!verify && !!req.session.jwt === false) { 
        res.send({message:"you already use this email please send authenticated E-Mail",done:true,changed:true})
    }
})

app.listen(process.env.PORT,(e) => {
    console.log("i work in " + process.env.PORT + "port")
})







// console.log(new Date("2022-05-21T05:14:43.000Z").toLocaleString());
// res.setHeader('Content-Type', 'application/json');
// res.cookie('"myapp-userid"',55, { maxAge: 10000});


